# Filmm App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

To get the frontend running locally:

- Clone this repo: `git clone git@bitbucket.org:lara_pont/filmm.git`
- `npm install` to install all required dependencies
- `npm start` to start the local server (this project uses create-react-app)

Open [http://localhost:3000](http://localhost:3000) to view it in the browser