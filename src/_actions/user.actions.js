import md5 from 'md5'
import { userService } from '_services'
import { history } from '_helpers'
import { alertActions } from '_actions'

export const userActions = {
  login,
  logout
}

function login (username, password, from) {
  return dispatch => {
    dispatch(request({ username }))

    userService.login({ user: username, pass: md5(password), device: 'Web' })
      .then(
        response => {
          if (!response.error) {
            dispatch(success(response))
            history.push(from)
          } else {
            dispatch(alertActions.error(response.message))
            dispatch(failure(response.message))
          }
        },
        error => {
          dispatch(alertActions.error(error.toString()))
          dispatch(failure(error.toString()))
        }
      )
  }

  function request (user) { return { type: 'USERS_LOGIN_REQUEST', user } }
  function success (response) { return { type: 'USERS_LOGIN_SUCCESS', response } }
  function failure (error) { return { type: 'USERS_LOGIN_FAILURE', error } }
}

function logout (params) {
  userService.logout()
  return { type: 'USERS_LOGOUT' }
}
