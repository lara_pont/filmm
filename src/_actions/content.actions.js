import { mainService, userService } from '_services'

export const contentActions = {
  addToFavorites,
  filterByTerm,
  getContent,
  getFilm
}

function getContent () {
  return dispatch => {
    const token = userService.getToken()
    dispatch(request())
    mainService.getView(token)
      .then(
        response => {
          if (!response.error) {
            dispatch(success(response))
            // history.push(from)
          } else {
            dispatch(failure(response.message))
          }
        },
        error => {
          dispatch(failure(error.toString()))
        }
      )
  }

  function request () { return { type: 'CONTENT_REQUEST' } }
  function success (response) { return { type: 'CONTENT_SUCCESS', response } }
  function failure (error) { return { type: 'CONTENT_FAILURE', error } }
}

function getFilm (id) {
  return dispatch => {
    const token = userService.getToken()
    dispatch(request())

    mainService.getFilm(token, id)
      .then(
        response => {
          if (!response.error) {
            dispatch(success(response))
            // history.push(from)
          } else {
            dispatch(failure(response.message))
          }
        },
        error => {
          dispatch(failure(error.toString()))
        }
      )
  }

  function request () { return { type: 'FILM_REQUEST' } }
  function success (response) { return { type: 'FILM_SUCCESS', response } }
  function failure (error) { return { type: 'FILM_FAILURE', error } }
}

function filterByTerm (response) {
  return dispatch => {
    dispatch({ type: 'FILTER_BY_TERM', response })
  }
}

function addToFavorites (response) {
  return dispatch => {
    dispatch({ type: 'ADD_TO_FAVORITES', response })
  }
}
