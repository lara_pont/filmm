const initialState = {
  content: [],
  contentFiltered: [],
  user: {},
  error: null,
  loading: false,
  film: {}
}

export function data (state = initialState, action) {
  switch (action.type) {
    case 'CONTENT_REQUEST':
      return {
        ...state,
        loading: true,
        film: {}
      }
    case 'CONTENT_SUCCESS':
      return {
        ...state,
        content: action.response.contents,
        contentFiltered: action.response.contents,
        user: action.response.user,
        loading: false,
        error: false,
        film: {}
      }
    case 'CONTENT_FAILURE':
      return {
        ...state,
        content: [],
        contentFiltered: [],
        user: [],
        error: true,
        loading: false,
        film: {}
      }
    case 'FILM_REQUEST':
      return {
        ...state,
        loading: true,
        film: {}
      }
    case 'FILM_SUCCESS':
      return {
        ...state,
        loading: false,
        error: false,
        film: action.response
      }
    case 'FILM_FAILURE':
      return {
        ...state,
        loading: false,
        error: true,
        film: {}
      }
    case 'FILTER_BY_TERM':
      return {
        ...state,
        contentFiltered: action.response.filtered
      }
    case 'ADD_TO_FAVORITES':
      return {
        ...state,
        user: {
          ...state['user'],
          favs: action.response.favorites
        }
      }
    default:
      return state
  }
}
