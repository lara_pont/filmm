import { combineReducers } from 'redux'

import { alert } from './alert.reducer'
import { authentication } from './authentication.reducer'
import { data } from './content.reducer'

export default combineReducers({
  alert,
  authentication,
  data
})
