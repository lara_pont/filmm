import React, { useEffect } from 'react'
import { Redirect, Link } from 'react-router-dom'
import { FcFilmReel } from 'react-icons/fc'

import { Login } from '_components'
import { userService } from '_services'
import styles from './LoginPage.module.scss'

const LoginPage = (props) => {
  useEffect(() => {
    if (userService.loggedIn()) {
      return <Redirect to='/' />
    }
  }, [])

  return (
    <>
      <Link className={styles['logo']} to='/'>
        <FcFilmReel size='2em' /> <span>Filmms</span>
      </Link>
      <Login {...props} />
    </>
  )
}

export default LoginPage
