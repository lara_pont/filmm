import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { contentActions } from '_actions'
import { FilmItem } from '_components'

import styles from './HomePage.module.scss'

const HomePage = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(contentActions.getContent())
  }, [])

  const loading = useSelector(state => state.data.loading)
  const error = useSelector(state => state.data.error)
  const contentFiltered = useSelector(state => state.data.contentFiltered)

  return (
    <>
      { error ? <div className='error'>Hubo un error...</div> : null }
      <h1 className={styles['title']}>Catálogo</h1>
      { loading ? 'Cargando...' : null }
      {
        contentFiltered && contentFiltered.length
          ? <section className={styles['container']}>
            { contentFiltered.map(item => <FilmItem key={item.id} item={item} />) }
          </section>
          : null
      }
    </>

  )
}

export default HomePage
