import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import ReactPlayer from 'react-player'

import { contentActions } from '_actions'
import styles from './FilmPage.module.scss'

const FilmPage = ({ match }) => {
  const dispatch = useDispatch()
  const { id } = match.params

  useEffect(() => {
    dispatch(contentActions.getFilm(id))
  }, [])

  const loading = useSelector(state => state.data.loading)
  const error = useSelector(state => state.data.error)
  const film = useSelector(state => state.data.film)

  return (

    <>
      { error ? <div className='error'>Hubo un error...</div> : null }

      { loading ? 'Cargando...' : null }
      {
        film !== undefined
          ? <div>
            <section className={styles['film']}>
              <div className={styles['info']}>
                <img className={styles['cover']} src={film.cover} alt={film.title} />
                <div className={styles['additional-info']}>
                  <h1 className={styles['name']}>{film.title}</h1>
                  <p>
                    <span className={styles['section']}>{film.section}</span>
                    <span className={styles['duration']}>{ Math.round(film.duration / 60)} min.</span>
                    <span className={styles['rating']}>{film.rating}</span>
                  </p>
                  {film.votes} -
                  {film.totalVotes}
                </div>
              </div>

              {/*
                Votos: {film.votes}
                TotalVotos: {film.totalVotes} */}
            </section>
            <section className={styles['player']}>
              <ReactPlayer playing controls config={{
                file: {
                  forceHLS: true
                }
              }} url={film.url} width={'auto'} />
            </section>
          </div>
          : null
      }

    </>

  )
}

export default FilmPage
