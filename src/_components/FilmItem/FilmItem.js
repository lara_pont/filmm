import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { IconContext } from 'react-icons'
import { FiHeart } from 'react-icons/fi'

import { contentActions } from '_actions'
import styles from './FilmItem.module.scss'

const FilmItem = ({ item }) => {
  const history = useHistory()
  const [favorites, setFavorites] = useState([])
  const stateFav = useSelector(state => state.data.user.favs)
  const dispatch = useDispatch()

  useEffect(() => {
    if (favorites.length > 0) {
      dispatch(contentActions.addToFavorites({ favorites }))
    }
  }, [favorites])

  const redirectToPage = id => {
    history.push(`/film/${item.id}`)
  }

  const addFav = (e, i) => {
    e.stopPropagation()
    const arr = [...stateFav]
    let addArray = true

    // Remove allways
    arr.forEach((item, key) => {
      if (item === i) {
        arr.splice(key, 1)
        addArray = false
      }
    })

    if (addArray) arr.push(i)
    setFavorites([...arr])
  }

  if (!item) return 'Cargando...'
  return (
    <div onClick={() => redirectToPage(item.id)} className={styles['item']}>
      <img src={item.cover} alt={item.title} />
      <h2>{item.title}</h2>
      {!stateFav.includes(item.id)
        ? <IconContext.Provider value={{ className: `${styles['fav-icon']} ${styles['fav-icon-empty']}` }}>
          <FiHeart onClick={(e) => addFav(e, item.id)} />
        </IconContext.Provider>
        : <IconContext.Provider value={{ className: `${styles['fav-icon']} ${styles['fav-icon-fill']}` }}>
          <FiHeart onClick={(e) => addFav(e, item.id)} />
        </IconContext.Provider>
      }
    </div>
  )
}

export default FilmItem

FilmItem.diplayName = 'FilmItem'
FilmItem.propTypes = {
  item: PropTypes.exact({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    cover: PropTypes.string.isRequired,
    duration: PropTypes.number,
    url: PropTypes.string,
    section: PropTypes.string
  })
}
