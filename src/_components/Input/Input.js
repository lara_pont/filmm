import React, { useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import styles from './Input.module.scss'

export default function Input ({ id, type, placeholder, className, required, disabled, valor, onChange, onFocus, onBlur }) {
  const [active, setActive] = useState(false)
  const [value, setValue] = useState(valor || '')

  const handleFocus = (e) => {
    setActive(true)
  }

  const handleBlur = e => {
    const inputValue = e.target.value
    if (inputValue === '') {
      setActive(false)
    }
  }

  const handleChange = e => {
    setValue(e.target.value)
  }

  const inputStyles = classNames({
    [styles['input']]: true,
    [className]: className !== undefined,
    [styles['focused']]: active === true
  })

  return (
    <input
      id={id}
      name={id}
      placeholder={placeholder}
      type={type}
      className={inputStyles}
      onChange={e => { onChange(e); handleChange(e) }}
      onFocus={(e) => { onFocus(e); handleFocus(e) }}
      onBlur={(e) => { onBlur(e); handleBlur(e) }}
      value={value}
      required={required}
      disabled={disabled}
      autoComplete='off'
      autoCorrect='off'
    />
  )
}

Input.diplayName = 'Input'
Input.propTypes = {
  id: PropTypes.string.isRequired,
  /** Additional classes */
  className: PropTypes.string,
  type: PropTypes.oneOf(['text', 'password', 'email', 'number', 'tel', 'url', 'search']),
  /** Value of input */
  valor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  required: PropTypes.bool,
  /** Specifies a short hint that describes the expected value of a text area */
  placeholder: PropTypes.string,
  /** The onChange event occurs when the value of an element has been changed. */
  onChange: PropTypes.func,
  /** The onFocus event occurs when an element gets focus. */
  onFocus: PropTypes.func,
  /** The onBlur event occurs when an object loses focus. */
  onBlur: PropTypes.func,
  /** The onKeyDown event occurs when the user is pressing a key (on the keyboard). */
  onKeyDown: PropTypes.func
}

Input.defaultProps = {
  type: 'text',
  required: true,
  onChange: () => { },
  onFocus: () => { },
  onBlur: () => { },
  onKeyDown: () => { }
}
