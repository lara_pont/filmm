import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { IconContext } from 'react-icons'
import { VscLoading } from 'react-icons/vsc'

import styles from './Button.module.scss'

export default function Button ({ children, isLoading, type, onClick, className, disabled, skin }) {
  const btnStyles = classNames(className,
    {
      [styles['btn-primary']]: skin === 'primary',
      [styles['btn-secondary']]: skin === 'secondary',
      [className]: className !== undefined
    })

  return (
    <button className={btnStyles} type={type} onClick={onClick} disabled={disabled}>
      {isLoading &&
        <span className='spinner'>
          <IconContext.Provider value={{ className: styles['loading'] }}>
            <VscLoading />
          </IconContext.Provider>
        </span>
      }
      {children}
    </button>
  )
}

Button.diplayName = 'Button'
Button.propTypes = {
  type: PropTypes.oneOf(['button', 'reset', 'submit']),
  /** String based node */
  children: PropTypes.node,
  /** Additional classes */
  className: PropTypes.string,
  /** Click event handler */
  onClick: PropTypes.func,
  /** Applies disabled styles */
  disabled: PropTypes.bool,
  /** Applies loading styles */
  isLoading: PropTypes.bool,
  /** Skins of Button content */
  skin: PropTypes.oneOf([
    'primary',
    'secondary'
  ])
}

Button.defaultProps = {
  type: 'button',
  onClick: () => { },
  isLoading: false,
  skin: 'primary'
}
