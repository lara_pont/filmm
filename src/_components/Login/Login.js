import React, { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import { userActions, alertActions } from '_actions'
import { Input, Button, Alert } from '_components'

import styles from './Login.module.scss'

const Login = () => {
  const [inputs, setInputs] = useState({
    username: '',
    password: ''
  })
  const [submitted, setSubmitted] = useState(false)
  const { username, password } = inputs
  const loggingIn = useSelector(state => state.authentication.loggingIn)
  const alert = useSelector(state => state.alert)

  const dispatch = useDispatch()
  const location = useLocation()

  // reset login status
  useEffect(() => {
    dispatch(userActions.logout())
    dispatch(alertActions.clear())
  }, [])

  function handleChange (e) {
    const { name, value } = e.target
    setInputs(inputs => ({ ...inputs, [name]: value }))
  }

  function handleSubmit (e) {
    e.preventDefault()

    setSubmitted(true)
    if (username && password) {
      const { from } = location.state || { from: { pathname: '/' } }
      dispatch(userActions.login(username, password, from))
    }
  }

  return (
    <div className={`${styles['container']} mt12`}>
      <h1 className='mb5'>Iniciar sesión</h1>
      <form className={styles['loginForm']} name='loginForm' onSubmit={handleSubmit}>

        <Input id='username' placeholder='email' onChange={handleChange} value={username} />
        {submitted && !username &&
        <div className={styles['invalid']}>Email is required</div>
        }

        <Input id='password' className='mt3' type='password' placeholder='password' onChange={handleChange} value={password} />
        {submitted && !password &&
        <div className={styles['invalid']}>Password is required</div>
        }

        <Button type='submit' className='mt3' isLoading={loggingIn}>Entrar</Button>
      </form>
      { alert.message && <Alert alert={alert} /> }
    </div>
  )
}

export default Login
