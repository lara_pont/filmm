import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'

import style from './UserNav.module.scss'

const UserNav = () => {
  const [isOpenMenu, setIsOpenMenu] = useState(false)
  const history = useHistory()

  const user = useSelector(state => state.data.user)

  const handleClick = () => {
    setIsOpenMenu(false)
    history.push(`/login`)
  }

  return (
    <div className={style['menu-container']} onClick={() => setIsOpenMenu(!isOpenMenu)}>
      <div className='menuButton'>
        <img className={style['avatar']} src={user.avatar} alt={user.name} title={user.name} />
      </div>
      <ul className={style['menu']} hidden={!isOpenMenu}>
        <li className={style['item']} onClick={() => handleClick()}>
          Logout
        </li>
      </ul>
    </div>
  )
}

export default UserNav
