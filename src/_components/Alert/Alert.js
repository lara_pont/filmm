import React from 'react'
import PropTypes from 'prop-types'
import styles from './Alert.module.scss'

const Alert = ({ alert }) => {
  const { message, type } = alert
  return (
    <>
      {message &&
      <div className={`${styles['alert']} ${styles[type]}`}>{message}</div>
      }
    </>
  )
}

export default Alert

Alert.diplayName = 'Alert'
Alert.propTypes = {
  alert: PropTypes.exact({
    message: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['alert-success', 'alert-danger'])
  })
}
