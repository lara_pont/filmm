import React from 'react'
import { Link } from 'react-router-dom'
import { FcFilmReel } from 'react-icons/fc'

import { UserNav, Searcher } from '_components'
import style from './Header.module.scss'

const Header = () => {
  return (
    <header className={style['header']}>
      <Link className={style['brand']} to='/'>
        <FcFilmReel size='2em' /> <span>Filmms</span>
      </Link>
      <Searcher placeholder={'Busca por título'} />
      <UserNav />
    </header>
  )
}

export default Header
