import React, { useRef, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { IconContext } from 'react-icons'
import { FiSearch } from 'react-icons/fi'
import { contentActions } from '_actions'

import styles from './Searcher.module.scss'

const Searcher = ({ placeholder, className }) => {
  const [searchTerm, setSearchTerm] = useState('')
  const [searchResults, setSearchResults] = useState([])

  const inputSearch = useRef(null)
  const dispatch = useDispatch()
  const content = useSelector(state => state.data.content)

  useEffect(() => {
    const results = content.filter(film =>
      film.title.toLowerCase().includes(searchTerm)
    )
    setSearchResults(results)
  }, [searchTerm])

  useEffect(() => {
    if (searchTerm !== '') { // Input search is not empty
      dispatch(contentActions.filterByTerm({ filtered: searchResults }))
    } else {
      dispatch(contentActions.filterByTerm({ filtered: content }))
    }
  }, [searchResults])

  return (
    <div className={styles['input-container']}>
      <IconContext.Provider value={{ className: `${styles['icon']}` }}>
        <FiSearch />
      </IconContext.Provider>
      <input
        id='search'
        className={`${className} ${styles['input']}`}
        ref={inputSearch}
        type='text'
        placeholder={placeholder}
        value={searchTerm}
        autoComplete='off'
        onChange={event => setSearchTerm(event.target.value)}
      />
    </div>
  )
}

export default Searcher

Searcher.diplayName = 'Searcher'
Searcher.propTypes = {
  /** Placeholder to display */
  placeholder: PropTypes.string,
  /** A single CSS class name to be appended to the Input's wrapper element. */
  className: PropTypes.string
}

Searcher.defaultProps = {
  placeholder: 'Búscar'
}
