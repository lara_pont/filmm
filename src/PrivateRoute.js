import React from 'react'
import { Route, Redirect } from 'react-router-dom'

import {
  Header
} from '_components/'
import styles from './PrivateRoute.module.scss'

function PrivateRoute ({ component: Component, roles, ...rest }) {
  return (
    <Route {...rest} render={props => {
      if (!window.localStorage.getItem('token')) {
        // not logged in so redirect to login page with the return url
        return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      }
      return (
        <>
          <Header />
          <main className={styles['content']}>
            <div className={styles['container']}>
              <Component {...props} />
            </div>
          </main>
        </>
      )
    }} />
  )
}

export { PrivateRoute }
