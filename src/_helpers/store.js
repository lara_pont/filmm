import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../_reducers'

let middlewares = []
middlewares = [...middlewares, thunkMiddleware]

export const store = createStore(
  rootReducer,
  compose(applyMiddleware(...middlewares))
)
