import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import axios from 'axios'

import { store } from './_helpers'
import App from './App'
import reportWebVitals from './reportWebVitals'

import './index.scss'

axios.interceptors.request.use(async request => {
  request.baseURL = await process.env.REACT_APP_API
  // if (!request.headers.Authorization) {
  //   const token = window.localStorage.getItem('token')
  //   if (token) {
  //     request.headers.Authorization = `${token}`
  //   } else {
  //     delete request.headers.Authorization
  //   }
  // }
  return request
}, error => Promise.reject(error))

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
