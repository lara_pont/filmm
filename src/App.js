import React from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import {
  FilmPage,
  HomePage,
  LoginPage
} from 'views/'

import { PrivateRoute } from './PrivateRoute'

import { history } from '_helpers'
import styles from './App.module.scss'

const App = () => {
  return (
    <div className={styles['container']}>
      <Router history={history}>
        <Switch>
          <PrivateRoute exact path='/' component={HomePage} />
          <PrivateRoute exact path='/film/:id' component={FilmPage} />
          <Route path='/login' component={LoginPage} />
          <Redirect from='*' to='/' />
        </Switch>
      </Router>
    </div>
  )
}

export default App
