import axios from 'axios'
import queryString from 'query-string'

const headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

function login (user) {
  const params = queryString.stringify(user, { encode: false })
  return axios.post('Login.php', params, { headers })
    .then(response => {
      const token = response.data.token
      setToken(token)
      window.localStorage.setItem('user', user.user)
      return response.data
    })
    .catch((err) => {
      console.log('AXIOS ERROR: ', err)
    })
}

function loggedIn () {
  const token = getToken()
  return token
}

function setToken (token) {
  window.localStorage.setItem('token', token)
  return token
}

function getToken () {
  return window.localStorage.getItem('token')
}

function logout () {
  window.localStorage.clear()
}

export const userService = {
  getToken,
  login,
  loggedIn,
  logout,
  setToken
}
