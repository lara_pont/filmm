import axios from 'axios'
import queryString from 'query-string'

const headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

export const mainService = {
  getView,
  getFilm
}

function getView (token) {
  const params = queryString.stringify({
    token,
    device: 'Web'
  }, { encode: false })

  return axios.post('GetView.php', params, { headers })
    .then(response => response.data)
    .catch((err) => {
      console.log('AXIOS ERROR: ', err)
    })
}

function getFilm (token, id) {
  const params = queryString.stringify({
    token,
    device: 'Web',
    id
  }, { encode: false })

  return axios.post('Play.php', params, { headers })
    .then(response => response.data)
    .catch((err) => {
      console.log('AXIOS ERROR: ', err)
    })
}
